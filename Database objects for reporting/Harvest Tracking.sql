-- TONY'S SQL
-- This SQL retrieves the following for Estimates not delivered to the Estimate Planned Receival Point
-- Identifies tonnes and receiver of non-estimated commodities (20190606 1130)
-- 1. Estimated Total, 
-- 2. Estimated Delivery Site and 
-- 3. Actual Delivery Site


SELECT
loddlv [Estimator Account] , 
[Estimator Property],
[Estimate Planned Season],
[Estimate Planned Receival Area], 
[Estimate Planned Receival Site],
[Estimate Planned Receival Site Name],  
--loddlv [Actual Receival Account No], 
LODSTE [Actual Receival Site],
t2.stenme [Actual Receival Site Name], --[Actual Delivered Site],
lvacdy [Commodity], 
lvavar [Variety],
[Estimate Tonnes],
0 [Received Tonnes with Estimate], 
0 [Tonnes Delivered Elsewhere],
ROUND(SUM((CASE LWTWTY WHEN 'WT' THEN LWTWGT * -1 WHEN 'WG' THEN LWTWGT ELSE 0 END)* LVAPCT / 100),0) [Unestimated Received Tonnes]

FROM

(SELECT
eysssn [Estimate Planned Season],
t1.stedst [Estimate Planned Receival Area],
f.efgpro [Estimator Property],  -- Property where estimate originally comes from
eyfcdy [Estimate Commodity],
efgvar [Estimate Commodity Variety],
SUM(EFGDPH * ISNULL(EYFFAC,1)) AS [Estimate Tonnes], 

-- Site which delivery was intended.
f.efgprp [Estimate Planned Receival Site],
t1.stenme [Estimate Planned Receival Site Name],

-- Actual Site where delivery occured.
pstste [Actual Deliverer Site Code (Elsewhere)]

FROM
[CBH_DataStore].[IBIS].propst  p

LEFT OUTER JOIN [CBH_DataStore].[IBIS].ESTEYS eys on 
eysste = PSTSTE
AND eys.IS_CURRENT = 1

LEFT OUTER JOIN [CBH_DataStore].[IBIS].ESTEYG eyg on  
EYGDST = EYSDST 
AND EYGCDE = EYSEYG 
AND EYGSTS = 'A' 
AND eysssn = eygssn 
AND eyg.IS_CURRENT = 1

LEFT OUTER JOIN [CBH_DataStore].[IBIS].ESTEYF eyf on  
EYFDST = EYSDST 
AND EYFEYG = EYSEYG  
AND eyfssn = eysssn 
AND eyf.IS_CURRENT = 1

JOIN [CBH_DataStore].[IBIS].ESTEFG f on 
eysssn = efgssn 
AND eyfcdy = efgcdy 
AND f.IS_CURRENT = 1 
AND pstpro = efgpro 
AND PSTSTE = EYSSTE

LEFT OUTER JOIN [CBH_DataStore].[IBIS].SGLSTE t1 ON 
f.efgprp = t1.stecde 
AND t1.IS_CURRENT = 1 -- to get estimated receival site description

WHERE 
PSTSTO = 'NCR' 
AND p.IS_CURRENT = 1

GROUP BY
eysssn, 
t1.stedst,
t1.stenme,
eyfcdy , 
efgvar, 
efgpro,
efgprp,
pstste) AS estimatedtonnes 

JOIN [CBH_DataStore].[IBIS].rcglod l ON -- actual receivals
[Estimate Planned Season] = lodrsn 
AND [Estimator Property] = lodpro 
AND [Estimate Commodity] = lodcdy
AND l.IS_CURRENT = 1

JOIN [CBH_DataStore].[IBIS].rcglva v ON 
lvarsn = lodrsn 
AND lvaste = lodste 
AND lodnbr = lvalod 
AND lvavar = estimatedtonnes.[Estimate Commodity Variety]
AND v.IS_CURRENT = 1 -- load variety 

LEFT OUTER JOIN [CBH_DataStore].[IBIS].RCGLWT w ON 
LWTRSN = LODRSN 
AND LWTSTE = LODSTE 
AND LWTLOD = LODNBR 
AND w.IS_CURRENT = 1 -- load weight

LEFT OUTER JOIN [CBH_DataStore].[IBIS].SGLSTE t2 ON 
lodste = t2.stecde 
AND t2.IS_CURRENT = 1 -- to get receival site description


LEFT OUTER JOIN [CBH_DataStore].[IBIS].STCVAR a ON 
varcde = lvavar 
AND lvacdy = varcdy
AND a.IS_CURRENT = 1 -- to get variety description

WHERE
--lodrsn = '2019'
[Estimate Planned Season] = '2019'
AND lodspl = 0
AND  [Estimate Planned Receival Site] <> LODSTE

AND [Estimate Planned Receival Site] = 'CARNA'
AND [Estimate Commodity] = 'LUPINS'
AND [Estimate Commodity Variety] = 'COROMUP'

GROUP BY
[Estimate Planned Season],
[Estimate Planned Receival Area],  
LODSTE,
t2.stenme,
[Estimate Planned Receival Site], --EFGPRP Planned_Delivery, 
[Estimate Planned Receival Site Name],
loddlv, 
[Estimator Property], --efgpro, 
[Estimate Commodity],
lvacdy, 
[Estimate Commodity Variety],
lvavar,
[Estimate Tonnes] --total tonnes as estimated
