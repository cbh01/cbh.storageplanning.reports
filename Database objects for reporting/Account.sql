SELECT 
	 DISTINCT 
		 LODDLV AccountNo
		,CTCNMG [Given Name]
		,CTCNMF [Family Name]
		,LTRIM(RTRIM(CTCNMG)) + ' ' + LTRIM(RTRIM(CTCNMF)) [Primary Contact]
		,CTCPHN [Phone]
		,CTCMBL [Mobile]
		,CTCEML [Email Address]
FROM IBIS.RCGLOD L
LEFT OUTER JOIN IBIS.ADCETC ETC
	ON LODDLV = ETCENT
	AND ETCECN = 1
	AND ETC.IS_CURRENT = 1
LEFT OUTER JOIN IBIS.ADCCTC CTC
	ON CTCNBR = ETCCTC
	AND CTC.IS_CURRENT = 1
LEFT OUTER JOIN IBIS.SGLSTE STE
	ON RTRIM(LTRIM(LODSTE)) = RTRIM(LTRIM(STENME))
	AND STE.IS_CURRENT = 1
WHERE LODRSN = '2018'
AND LODSPL = 0
AND L.IS_CURRENT = 1