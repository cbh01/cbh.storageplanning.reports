SELECT 
	 SPFCDY Commodity
	,SPFCNF [Conversion Factor]
FROM [CBH_DataStore].[IBIS].[SPLSPF]
WHERE SPFSSN = 2018
AND IS_CURRENT = 1