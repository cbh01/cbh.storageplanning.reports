WITH GradeGroup_CTE
AS
(
	SELECT DISTINCT GGPDSC AS [Grade Group]
	FROM [CBH_DataStore].[IBIS].[STCGGP]
	WHERE IS_CURRENT = 1
)
SELECT
	 ROW_NUMBER() OVER (ORDER BY [Grade Group]) AS GradeGroupID
	,[Grade Group]
FROM GradeGroup_CTE
UNION ALL
SELECT '-1','UNK'